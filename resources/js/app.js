/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import InstantSearch from 'vue-instantsearch';

window.Vue = require('vue');

// Add localizations
import vue_langs from './translations.js';

import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: App['default_locale'],
    fallbackLocale: App['fallback_locale'],
    messages: vue_langs
});

Vue.use(InstantSearch);

let authorizations = require('./authorizations');

Vue.prototype.authorize = function (...params) {
    if (! window.App.user) return false;

    if (typeof params[0] === 'string') {
        return authorizations[params[0]](params[1]);
    }

    return params[0](window.App.user);
};

Vue.prototype.signedIn = window.App.user;

window.events = new Vue();

window.flash = function (message, level = 'success') {
    window.events.$emit('flash', {message, level});
}; // flash('my new flash message')

Vue.component('flash', require('./components/Flash.vue').default);
Vue.component('paginator', require('./components/Paginator.vue').default);
Vue.component('user-notifications', require('./components/UserNotifications.vue').default);
Vue.component('thread-view', require('./pages/Thread.vue').default);
Vue.component('avatar-form', require('./components/AvatarForm.vue').default);
Vue.component('instant-search', require('./components/InstantSearch').default);
Vue.component('wysiwyg', require('./components/Wysiwyg').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({ i18n }).$mount('#app');
