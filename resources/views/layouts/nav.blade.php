<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('messages.nav.toggle') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link{{ Request::is('threads') && Request::getQueryString() == '' ? ' active' : '' }}" href="{{ route('threads.index') }}">{{ __('messages.nav.all_threads') }}</a>
                </li>

                @auth
                    <li class="nav-item">
                        <a class="nav-link{{ Request::getQueryString() == 'by=' . auth()->user()->slug ? ' active' : '' }}" href="{{ route('threads.index') }}?by={{ auth()->user()->slug }}">{{ __('messages.nav.my_threads') }}</a>
                    </li>
                @endauth

                <li class="nav-item">
                    <a class="nav-link{{ Request::getQueryString() == 'popular=1' ? ' active' : '' }}" href="{{ route('threads.index') }}?popular=1">{{ __('messages.nav.popular_threads') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ Request::getQueryString() == 'unanswered=1' ? ' active' : '' }}" href="{{ route('threads.index') }}?unanswered=1">{{ __('messages.nav.unanswered_threads') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ Request::is('threads/create') ? ' active' : '' }}" href="{{ route('threads.create') }}">{{ __('messages.nav.new_thread') }}</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('messages.nav.channels') }}</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">

                        @foreach ($channels as $channel)
                            <a class="dropdown-item" href="/threads/{{ $channel->slug }}">{{ $channel->name }}</a>
                        @endforeach

                    </div>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('messages.nav.login') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('messages.nav.register') }}</a>
                        </li>
                    @endif
                @else
                    <user-notifications></user-notifications>

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ auth()->user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a href="{{ route('profile', auth()->user()) }}" class="dropdown-item">
                                {{ __('messages.nav.profile') }}
                            </a>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('messages.nav.logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
