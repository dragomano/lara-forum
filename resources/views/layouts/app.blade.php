<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}@stack('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('header')
</head>
<body>
    <div id="app">
        @include('layouts.nav')

        <nav aria-label="breadcrumb">
            @yield('breadcrumbs')
        </nav>

        <main class="mt-3 py-4">
            @yield('content')
        </main>

        <flash message="{{ session('flash') }}"></flash>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script>
        window.App = @json([
            'default_locale' => config('app.locale'),
            'fallback_locale' => config('app.fallback_locale'),
            'user' => Auth::user()
        ]);
    </script>

    @yield('footer')
</body>
</html>
