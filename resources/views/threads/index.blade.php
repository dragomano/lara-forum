@extends('layouts.app')

@push('title', ' - ' . __('messages.nav.all_threads'))

@section('breadcrumbs', Breadcrumbs::render('threads', Request::getQueryString()))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="row">
                    @include('threads._list')

                    {{ $threads->links() }}
                </div>
            </div>
            <div class="col-md-3">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    {{ __('messages.nav.search') }}
                </h4>
                <form class="mb-3" method="get" action="{{ route('threads.search') }}">
                    @csrf
                    <div class="input-group">
                        <input class="form-control" type="text" placeholder="{{ __('messages.nav.search_smth') }}..." name="q">
                        <button class="btn btn-secondary" type="submit">{{ __('messages.nav.search') }}</button>
                    </div>
                </form>

                @if ($count = count($trending))
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="text-muted">{{ __('messages.nav.trending_threads') }}</span>
                        <span class="badge bg-secondary rounded-pill">{{ $count }}</span>
                    </h4>
                    <ul class="list-group mb-3">
                        @foreach ($trending as $thread)
                            <li class="list-group-item d-flex justify-content-between">
                                <a href="{{ url($thread['path']) }}">
                                    {{ $thread['title'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif

            </div>
        </div>
    </div>
@endsection
