@forelse ($threads as $thread)
    <div class="col-md-4">
        <div class="media">

            @if ($thread->creator->avatar_path)
                <img class="mr-3 rounded-circle" src="{{ $thread->creator->avatar_path }}" width="40" height="40" alt="">
            @endif

            <div class="media-body">
                <h5 class="mt-2">
                    <a href="{{ $thread->path() }}">
                        @if (auth()->check() && $thread->hasUpdatesFor(auth()->user()))
                            <strong>
                                {{ $thread->title }}
                            </strong>
                        @else
                            {{ $thread->title }}
                        @endif
                    </a>
                </h5>
                <p class="small text-muted">
                    {{ __('messages.thread.posted_by') }} <a href="{{ route('profile', $thread->creator) }}">{{ $thread->creator->name }}</a>,
                    {{ $thread->created_at->diffForHumans() }}
                </p>
                {!! $thread->body !!}
                <p class="small text-muted">
                    {{ $thread->replies_count }} {{ Str::plural('reply', $thread->replies_count) }},
                    {{ $thread->visits }} {{ Str::plural('visit', $thread->visits) }}
                </p>
            </div>
        </div>
    </div>
@empty
    <div class="alert alert-light" role="alert">
        {{ __('messages.thread.no_results') }}
    </div>
@endforelse
