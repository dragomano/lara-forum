@extends('layouts.app')

@push('title', ' - ' . __('messages.nav.search'))

@section('breadcrumbs', Breadcrumbs::render('search'))

@section('content')
    <div class="container">
        <instant-search
            app-id="{{ config('scout.algolia.id') }}"
            api-key="{{ config('scout.algolia.key') }}"
            q="{{ request('q') }}"
        ></instant-search>
    </div>
@endsection
