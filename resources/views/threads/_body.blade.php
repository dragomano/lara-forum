<div v-if="! editing">
    <h1 class="display-4 font-italic" v-text="title"></h1>
    <p>
        <img class="mr-1 rounded-circle" src="{{ $thread->creator->avatar_path }}" width="40" height="40" alt="{{ $thread->creator->name }}">
        <a href="{{ route('profile', $thread->creator) }}">{{ $thread->creator->name }}</a>,
        <small class="text-muted">{{ $thread->created_at->diffForHumans() }}</small>
    </p>

    <button class="float-right btn btn-outline-danger btn-sm" v-if="authorize('owns', thread)" @click="editing = true">{{ __('messages.actions.edit') }}</button>

    <p class="lead my-3" v-html="body"></p>
</div>

<div v-else>
    <label for="title">{{ __('messages.thread.title') }}</label>
    <input id="title" class="form-control mb-3" type="text" v-model="form.title" required>
    <label for="body">{{ __('messages.thread.body') }}</label>

    <wysiwyg v-model="form.body"></wysiwyg>

    @can('update', $thread)
        <div class="btn-group float-right" role="group" aria-label="Basic example">
            <button class="btn btn-primary btn-sm" @click="update">{{ __('messages.actions.update') }}</button>
            <button class="btn btn-warning btn-sm" @click="resetForm">{{ __('messages.actions.cancel') }}</button>

            <form method="post" action="{{ $thread->path() }}">
                @csrf
                @method('delete')

                <button class="btn btn-danger btn-sm" type="submit">
                    {{ __('messages.thread.delete') }}
                </button>
            </form>
        </div>
    @endcan

    <div class="clearfix mt-1 mb-1"></div>
</div>
