@extends('layouts.app')

@push('title', ' - ' . $thread->title)

@section('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.0/trix.min.css" integrity="sha512-5m1IeUDKtuFGvfgz32VVD0Jd/ySGX7xdLxhqemTmThxHdgqlgPdupWoSN8ThtUSLpAGBvA8DY2oO7jJCrGdxoA==" crossorigin="anonymous">
@endsection

@section('breadcrumbs', Breadcrumbs::render('thread', $thread))

@section('content')
    <thread-view :thread="{{ $thread }}" inline-template>
        <div class="container">
            <div class="row">

                <div class="col-md-8" v-cloak>
                    @include('threads._body')

                    <replies @added="repliesCount++" @removed="repliesCount--"></replies>
                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">
                                {!! __('messages.thread.info', [
                                    'date' => $thread->created_at->diffForHumans(),
                                    'author' => '<a href="' . route('profile', $thread->creator) . '">' . $thread->creator->name . '</a>',
                                    'replies' => '<span v-text="repliesCount"></span> ' . Str::plural('comment', $thread->replies->count())
                                ]) !!}
                            </p>
                            <p>
                                <subscribe-button :active="@json($thread->isSubscribedTo)" v-if="signedIn"></subscribe-button>

                                <button class="btn btn-outline-danger" v-if="authorize('isAdmin')" @click="toggleLock" v-text="locked ? '{{ __('messages.thread.unlock') }}' : '{{ __('messages.thread.lock') }}'"></button>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </thread-view>
@endsection
