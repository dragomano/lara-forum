@extends('layouts.app')

@push('title', ' - ' . __('messages.nav.new_thread'))

@section('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.0/trix.min.css" integrity="sha512-5m1IeUDKtuFGvfgz32VVD0Jd/ySGX7xdLxhqemTmThxHdgqlgPdupWoSN8ThtUSLpAGBvA8DY2oO7jJCrGdxoA==" crossorigin="anonymous">
@endsection

@section('breadcrumbs', Breadcrumbs::render('new_thread'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('messages.thread.create') }}</div>
                    <div class="card-body">

                        @if (count($errors))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif

                        <form id="create_form" method="post" action="{{ route('threads.store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="channel_id">{{ __('messages.thread.choose_channel') }}</label>
                                <select class="form-select" name="channel_id" id="channel_id" required>
                                    <option value="">{{ __('messages.thread.choose_one') }}...</option>

                                    @foreach ($channels as $channel)
                                        <option value="{{ $channel->id }}"{{ old('channel_id') == $channel->id ? ' selected' : '' }}>{{ $channel->name }}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">{{ __('messages.thread.title') }}</label>
                                <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="body">{{ __('messages.thread.body') }}</label>
                                <wysiwyg name="body" value="{{ old('body') }}"></wysiwyg>
                            </div>
                            <button class="btn btn-primary mt-2 g-recaptcha"
                                    data-sitekey="{{ env('RECAPTCHA_SITE_KEY') }}"
                                    data-callback="onSubmit"
                                    data-action="submit">{{ __('messages.thread.publish') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        function onSubmit(token) {
            document.getElementById("create_form").submit();
        }
    </script>
@endsection
