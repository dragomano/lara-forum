@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h4 class="mb-3">{{ __('Reset Password') }}</h4>

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="row g-3">

                    <div class="col-12">
                        <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
                        <div class="input-group">
                            <span class="input-group-text">@</span>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                        {{ __('Send Password Reset Link') }}
                    </button>

                </div>
            </form>
        </div>
    </div>
</div>
@endsection
