<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($months as $month)
        <sitemap>
            <loc>{{ route('sitemap.threads.show', $month->date) }}</loc>
            <lastmod>{{ $month->updated_at->tz('UTC')->toAtomString() }}</lastmod>
        </sitemap>
    @endforeach
</sitemapindex>
