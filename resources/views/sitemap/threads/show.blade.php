<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($threads as $thread)
        <url>
            <loc>{{ route('threads.show', [$thread->channel, $thread]) }}</loc>
            <lastmod>{{ $thread->updated_at->tz('UTC')->toAtomString() }}</lastmod>
        </url>
    @endforeach
</urlset>
