@extends('layouts.app')

@push('title', ' - ' . __('messages.nav.profile'))

@section('breadcrumbs', Breadcrumbs::render('profile', $profileUser))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <avatar-form :user="{{ $profileUser }}"></avatar-form>
            </div>
        </div>
        <div class="row">
            <ul class="list-unstyled mt-3">

            @forelse ($activities as $date => $activity)
                <h3 class="page-header">{{ $date }}</h3>

                @foreach ($activity as $record)
                    @if (view()->exists("profiles.activities.{$record->type}"))
                        @include ("profiles.activities.{$record->type}", ['activity' => $record])
                    @endif
                @endforeach

            @empty
                <p class="mt-3">{{ __('messages.profile.no_activity') }}</p>
            @endforelse

            </ul>
        </div>
    </div>
@stop
