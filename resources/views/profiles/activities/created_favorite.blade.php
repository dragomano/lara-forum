@component('profiles.activities.activity')
    @slot('heading')
        {{ $profileUser->name }} {{ __('messages.profile.favored') }} <a href="{{ $activity->subject->favorite->path() }}">{{ __('messages.profile.reply') }}</a>
    @endslot

    @slot('body')
        {!! $activity->subject->favorite->body !!}
    @endslot
@endcomponent
