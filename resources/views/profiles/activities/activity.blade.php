<li class="media mb-4">
    <div class="media-body">
        <h5 class="mt-0 mb-1">
            {{ $heading }}
        </h5>
        {{ $body }}
    </div>
</li>
