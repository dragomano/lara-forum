<?php

return [
    'actions' => [
        'post' => 'Post',
        'update' => 'Update',
        'cancel' => 'Cancel',
        'edit' => 'Edit',
        'delete' => 'Delete',
    ],

    'thread' => [
        'info' => 'This thread was published :date by :author, and currently has :replies.',
        'title' => 'Title',
        'body' => 'Body',
        'delete' => 'Delete thread',
        'posted_by' => 'Posted by',
        'no_results' => 'There are no relevant results at this time.',
        'create' => 'Create a new thread',
        'choose_channel' => 'Choose a channel',
        'choose_one' => 'Choose one',
        'subscribe' => 'Subscribe',
        'lock' => 'Lock',
        'unlock' => 'Unlock',
        'publish' => 'Publish',
        'best_reply' => 'Best reply',
        'please_signin' => 'Please <a href="/login">sign in</a> to participate in this discussion.',
        'is_locked' => 'This thread has been locked. No more replies are allowed.',
    ],

    'nav' => [
        'home' => 'Home',
        'toggle' => 'Toggle navigation',
        'all_threads' => 'All threads',
        'my_threads' => 'My threads',
        'popular_threads' => 'Popular threads',
        'unanswered_threads' => 'Unanswered threads',
        'new_thread' => 'New thread',
        'channels' => 'Channels',
        'login' => 'Login',
        'register' => 'Register',
        'profile' => 'Profile',
        'logout' => 'Logout',
        'trending_threads' => 'Trending threads',
        'search' => 'Search',
        'search_smth' => 'Search for something',
        'loading' => 'Loading',
        'filter_channel' => 'Filter by channel',
    ],

    'profile' => [
        'choose_file' => 'Choose file',
        'browse' => 'Browse',
        'favored' => 'favored a',
        'reply' => 'reply',
        'replied' => 'replied to',
        'published' => 'published',
        'no_activity' => 'There is no activity for this user yet.',
    ],

    'notifications' => [
        'avatar_uploaded' => 'Avatar uploaded!',
        'user_mentioned' => ':user mentioned you in :reply',
        'user_replied' => ':user replied to :thread',
        'updated' => 'Updated!',
        'thread_updated' => 'Your thread has been updated.',
        'reply_posted' => 'Your reply has been posted',
        'reply_deleted' => 'Reply deleted',
        'thread_locked' => 'Thread is locked',
        'reply_too_frequently' => 'You are replying too frequently. Please take a break.',
        'thread_published' => 'Your thread has been published!'
    ],

    'tests' => [
        'only_one_favorite_reply' => 'Did not expect to insert the same record set twice.',
        'notification_failed' => 'Failed notification on a reply adding',
        'thread_was_locked' => 'Failed asserting that the thread was locked.',
    ]
];
