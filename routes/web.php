<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes(['verify' => true]);

Route::post('/users/{user}/avatar', 'UserAvatarController')->whereNumber('id');

Route::name('threads.')->prefix('threads')->group(function () {
    Route::get('/search', 'SearchController@show')->name('search');
    Route::get('/', 'ThreadController@index')->name('index');
    Route::post('/', 'ThreadController@store')->name('store')->middleware('verified');
    Route::get('/create', 'ThreadController@create')->name('create');
    Route::get('/{channel}/{thread}', 'ThreadController@show')->name('show');
    Route::patch('/{channel}/{thread}', 'ThreadController@update')->name('update');
});

Route::prefix('threads')->group(function () {
    Route::delete('/{channel}/{thread}', 'ThreadController@destroy');
    Route::get('/{channel}', 'ThreadController@index');
    Route::get('/{channel}/{thread}/replies', 'ReplyController@index');
    Route::post('/{channel}/{thread}/replies', 'ReplyController@store');
    Route::post('/{channel}/{thread}/subscriptions', 'ThreadSubscriptionController@store');
    Route::delete('/{channel}/{thread}/subscriptions', 'ThreadSubscriptionController@destroy');
});

Route::name('threads.')->prefix('locked-threads')->group(function () {
    Route::post('/{thread}', 'LockedThreadController@store')->name('lock');
    Route::delete('/{thread}', 'LockedThreadController@destroy')->name('unlock');
});

Route::prefix('replies')->group(function () {
    Route::patch('/{reply}', 'ReplyController@update');
    Route::delete('/{reply}', 'ReplyController@destroy')->name('replies.destroy');
    Route::post('/{reply}/favorites', 'FavoriteController@store');
    Route::delete('/{reply}/favorites', 'FavoriteController@destroy');
    Route::post('/{reply}/best', 'BestReplyController@store')->name('bestReplies.store');
});

Route::prefix('profiles')->group(function () {
    Route::get('/{user}', 'ProfileController@show')->name('profile');
    Route::get('/{user}/notifications', 'UserNotificationController@index');
    Route::delete('/{user}/notifications/{notification}', 'UserNotificationController@destroy');
});

Route::get('/sitemap.xml', 'SitemapController')->name('sitemap.index');
Route::get('/sitemap/threads.xml', 'ThreadSitemapController@index')->name('sitemap.threads.index');
Route::get('/sitemap/threads/{date}.xml', 'ThreadSitemapController@show')
    ->name('sitemap.threads.show')
    ->where('date', '[0-9]{4}-[0-9]{2}');
