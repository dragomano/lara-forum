<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push(__('messages.nav.home'), route('home'));
});

// Home > Threads
Breadcrumbs::for('threads', function ($trail, $query) {
    $trail->parent('home');

    switch ($query) {
        case '':
            $title = __('messages.nav.all_threads');
            break;

        case 'popular=1':
            $title = __('messages.nav.popular_threads');
            break;

        case 'unanswered=1':
            $title = __('messages.nav.unanswered_threads');
            break;

        default:
            $title = __('messages.nav.my_threads');
    }

    $trail->push($title, route('threads.index'));
});

// Home > Threads > New thread
Breadcrumbs::for('new_thread', function ($trail) {
    $trail->parent('threads', '');

    $trail->push(__('messages.nav.new_thread'), route('threads.create'));
});

// Home > Threads > Thread
Breadcrumbs::for('thread', function ($trail, $thread) {
    $trail->parent('threads', '');

    $trail->push($thread->title, route('threads.show', [$thread->channel, $thread]));
});

// Home > Threads > Search
Breadcrumbs::for('search', function ($trail) {
    $trail->parent('threads', '');

    $trail->push(__('messages.nav.search'));
});

// Home > Profile
Breadcrumbs::for('profile', function ($trail, $user) {
    $trail->parent('home');

    $trail->push($user->name);
});
