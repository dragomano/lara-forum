<?php

namespace Database\Seeders;

use App\Models\Reply;
use App\Models\Thread;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Создадим стандартные роли
        $this->call(RolesTableSeeder::class);

        // Создадим администратора и пользователя по умолчанию
        $this->call(UsersTableSeeder::class);

        // Создадим 30 случайных тем
        $threads = Thread::factory(30)->create();

        // Создадим по 10 ответов в каждой из созданных тем
        $threads->each(function ($thread) {
            Reply::factory(10)->create(['thread_id' => $thread->id]);
        });
    }
}
