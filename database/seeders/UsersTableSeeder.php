<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'slug' => 'admin',
            'role_id' => 1,
            'name' => 'Admin',
            'email' => 'a@a.test',
            'email_verified_at' => now(),
            'password' => bcrypt('a@a.test'),
            'api_token' => Str::random(80),
            'created_at' => now()
        ]);

        User::create([
            'slug' => 'user',
            'role_id' => 3,
            'name' => 'User',
            'email' => 'u@u.test',
            'email_verified_at' => now(),
            'password' => bcrypt('u@u.test'),
            'api_token' => Str::random(80),
            'created_at' => now()
        ]);
    }
}
