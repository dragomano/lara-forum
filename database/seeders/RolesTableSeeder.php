<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name'        => 'admin',
            'description' => __('Administrator')
        ]);

        Role::create([
            'name'        => 'moderator',
            'description' => __('Moderator')
        ]);

        Role::create([
            'name'        => 'user',
            'description' => __('User')
        ]);
    }
}
