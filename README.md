# Lara Forum

По мотивам курса
[Let's Build A Forum with Laravel and TDD](https://laracasts.com/series/lets-build-a-forum-with-laravel)

## Ингредиенты:

* Laravel 8.16
* Bootstrap 5 with icons
* Vue
* Recaptcha v3
* Laravel Scout with Algolia or ElasticSearch
* Vue InstantSearch

## Запуск проекта

Устанавливаем зависимости PHP:

`composer install --optimize-autoloader` (для разработки) или `composer install --optimize-autoloader --no-dev` (для продакшена)

Устанавливаем зависимости JavaScript:

`npm i`

Компилируем скрипты:

`npm run dev` (для разработки) или `npm run prod` (для продакшена)

Копируем `.env.example` в `.env`. Меняем требуемые настройки, при необходимости (например, `APP_ENV=production`, `APP_DEBUG=false` и т. п.)

Создаем новый ключ приложения:

`php artisan key:generate`

Создаем таблицы в базе данных с одновременным заполнением демо-данными:

`php artisan migrate:fresh --seed`

Копируем данные из таблицы `threads` в поисковый индекс:

`php artisan scout:import "App\Models\Thread"`

Тестирование (в версии для разработки):

`php artisan test`, `vendor\bin\phpunit --testdox` (с отображением результатов в формате TestDox), или `vendor\bin\paratest` (с параллельным запуском нескольких тестов)

## Настройки, которые требуется указать в .env

### Чтобы работала [Recaptcha](https://www.google.com/recaptcha)
* RECAPTCHA_SITE_KEY
* RECAPTCHA_SECRET_KEY

### Выбор поискового драйвера
* SCOUT_DRIVER=algolia или SCOUT_DRIVER=elastic

### Чтобы работал полнотекстовый поиск [Algolia](https://www.algolia.com)
* ALGOLIA_APP_ID
* ALGOLIA_APP_KEY
* ALGOLIA_SECRET

### Чтобы работала отправка имейлов
* MAIL_HOST
* MAIL_PORT
* MAIL_USERNAME
* MAIL_PASSWORD
* MAIL_ENCRYPTION
* MAIL_FROM_ADDRESS

### Чтобы открыть публичный доступ к загружаемым файлам
* FILESYSTEM_DRIVER=public

Создаем символьную ссылку, чтобы сделать загружаемые файлы доступными через веб:

`php artisan storage:link`

Кэшируем настройки, маршруты и переменные окружения (на продакшене):

`php artisan optimize`
