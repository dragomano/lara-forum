<?php

namespace App\Http\Controllers;

use App\Models\User;

class UserNotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return auth()->user()->unreadNotifications;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @param string $id
     * @return void
     */
    public function destroy(User $user, string $id)
    {
        $user->notifications()
            ->findOrFail($id)
            ->markAsRead();
    }
}
