<?php

namespace App\Http\Controllers;

use App\Models\Thread;

class LockedThreadController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @param Thread $thread
     * @return void
     */
    public function store(Thread $thread)
    {
        $thread->update(['locked' => true]);
    }

    /**
     * @param Thread $thread
     * @return void
     */
    public function destroy(Thread $thread)
    {
        $thread->update(['locked' => false]);
    }
}
