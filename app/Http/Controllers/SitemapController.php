<?php

namespace App\Http\Controllers;

use App\Models\Thread;

class SitemapController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $thread = Thread::orderBy('updated_at', 'desc')->first();

        return response()
            ->view('sitemap.index', compact('thread'))
            ->header('Content-Type', 'text/xml');
    }
}
