<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Reply;
use App\Models\Thread;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class ReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param $channel
     * @param Thread $thread
     * @return LengthAwarePaginator
     */
    public function index($channel, Thread $thread)
    {
        return $thread->replies()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreatePostRequest $request
     * @param $channel
     * @param Thread $thread
     * @return Reply|Application|ResponseFactory|Model|RedirectResponse|Response|void
     * @throws Exception
     */
    public function store(CreatePostRequest $request, $channel, Thread $thread)
    {
        if ($thread->locked) {
            return response(__('messages.notifications.thread_locked'), 422);
        }

        return $thread->addReply($request->validated())
            ->load('owner');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePostRequest $request
     * @param Reply $reply
     * @return Application|ResponseFactory|Response|void
     */
    public function update(UpdatePostRequest $request, Reply $reply)
    {
        $reply->update($request->validated());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Reply $reply
     * @return Application|ResponseFactory|RedirectResponse|Response
     * @throws AuthorizationException
     * @throws Exception
     */
    public function destroy(Reply $reply)
    {
        $this->authorize('update', $reply);

        $reply->delete();

        if (request()->expectsJson()) {
            return response(['status' => __('messages.notifications.reply_deleted')]);
        }

        return back();
    }
}
