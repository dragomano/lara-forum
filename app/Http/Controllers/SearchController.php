<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use Illuminate\Contracts\{
    Foundation\Application,
    Pagination\LengthAwarePaginator,
    View\Factory,
    View\View
};

class SearchController extends Controller
{
    /**
     * @return Application|LengthAwarePaginator|Factory|View
     * @return void
     */
    public function show()
    {
        if (request()->expectsJson()) {
            return Thread::search(request('q'))->paginate(25);
        }

        return view('threads.search');
    }
}
