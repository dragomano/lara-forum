<?php

namespace App\Http\Controllers;

use App\Models\Thread;

class ThreadSitemapController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $months = Thread::selectRaw('DATE_FORMAT(`updated_at`, "%Y-%m") AS `date`, MAX(`updated_at`) AS `updated_at`')
            ->groupBy('date')
            ->get();

        return response()->view('sitemap.threads.index', compact('months'))
            ->header('Content-Type', 'text/xml');
    }

    /**
     * @param string $date
     * @return \Illuminate\Http\Response
     */
    public function show(string $date)
    {
        $threads = Thread::select(['id', 'slug', 'channel_id', 'updated_at'])
            ->whereRaw('DATE_FORMAT(`updated_at`, "%Y-%m") = ?', [$date])
            ->orderBy('updated_at', 'desc')
            ->get();

        return response()->view('sitemap.threads.show', compact('threads'))
            ->header('Content-Type', 'text/xml');
    }
}
