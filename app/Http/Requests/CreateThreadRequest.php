<?php

namespace App\Http\Requests;

use App\Rules\Recaptcha;
use Illuminate\Foundation\Http\FormRequest;

class CreateThreadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Recaptcha $recaptcha
     * @return array
     */
    public function rules(Recaptcha $recaptcha)
    {
        return [
            'channel_id' => 'required|exists:channels,id',
            'title' => 'required|antispam',
            'body' => 'required|antispam',
            'g-recaptcha-response' => ['required', $recaptcha]
        ];
    }

    public function validated()
    {
        return [
            'user_id' => auth()->id(),
            'channel_id' => request('channel_id'),
            'title' => request('title'),
            'body' => request('body')
        ];
    }
}
