<?php

namespace App\Models;

trait Favorable
{
    protected static function bootFavorable()
    {
        static::deleting(function ($model) {
            $model->favorites->each->delete();
        });
    }

    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }

    public function isFavorite()
    {
        return $this->favorites->where('user_id', auth()->id())->count();
    }

    public function getIsFavoriteAttribute()
    {
        return $this->isFavorite();
    }

    public function favorite()
    {
        $attributes = ['user_id' => auth()->id()];

        if (! $this->favorites()->where($attributes)->exists()) {
            $this->favorites()->create($attributes);
        }
    }

    public function unfavorite()
    {
        $attributes = ['user_id' => auth()->id()];

        $this->favorites()->where($attributes)->get()->each->delete();
    }

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favorite');
    }
}
