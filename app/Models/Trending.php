<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;

class Trending
{
    public function get()
    {
        return array_slice(Cache::get($this->cacheKey(), []), 0, 5);
    }

    public function push($thread)
    {
        $collection = collect($this->get());
        $collection->push([
            'title' => $thread->title,
            'path' => $thread->path()
        ]);

        Cache::put($this->cacheKey(), $collection->unique()->toArray());
    }

    public function reset()
    {
        Cache::forget($this->cacheKey());
    }

    protected function cacheKey()
    {
        return app()->environment('testing') ? 'testing_trending_threads' : 'trending_threads';
    }
}
