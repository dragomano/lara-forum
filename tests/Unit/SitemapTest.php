<?php

namespace Tests\Unit;

use Tests\TestCase;

class SitemapTest extends TestCase
{
    public function testSitemapRouteIsAvailable()
    {
        $request = $this->get(route('sitemap.index'));

        $this->assertEquals("text/xml; charset=UTF-8", $request->headers->get('Content-Type'));
    }
}
