<?php

namespace Tests\Unit;

use Tests\TestCase;

class ChannelTest extends TestCase
{
    public function testAChannelConsistsOfThreads()
    {
        $channel = create('Channel');

        $thread = create('Thread', ['channel_id' => $channel->id]);

        $this->assertTrue($channel->threads->contains($thread));
    }
}

