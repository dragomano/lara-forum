<?php

namespace Tests\Unit;

use Tests\TestCase;

class BreadcrumbTest extends TestCase
{
    public function testHomePageHasBreadcrumbs()
    {
        $this->get('/')
            ->assertSee('breadcrumb-item active');
    }
}

