<?php

namespace Tests\Unit;

use App\Inspections\Spam;
use Tests\TestCase;

class SpamTest extends TestCase
{
    public function testItValidatesSpam()
    {
        $spam = new Spam();

        $this->assertFalse($spam->detect('::some-text::'));

        $this->expectException('Exception');

        $spam->detect('yahoo customer support');
    }

    public function testItChecksForAnyKeyBeingHeldDown()
    {
        $spam = new Spam();

        $this->expectException('Exception');

        $spam->detect('Hello world aaaaaaa');
    }
}

