<?php

namespace Tests\Unit;

use Tests\TestCase;

class ReplyTest extends TestCase
{
    public function testIsAnOwner()
    {
        $reply = create('Reply');

        $this->assertInstanceOf('App\Models\User', $reply->owner);
    }

    public function testItKnowsIfItWasJustPublished()
    {
        $reply = create('Reply');

        $this->assertTrue($reply->wasJustPublished());

        $reply->created_at = now()->subMonth();

        $this->assertFalse($reply->wasJustPublished());
    }

    public function testItCanDetectAllMentionedUsersInTheBody()
    {
        $reply = create('Reply', [
            'body' => '@JaneDoe wants to talk to @JohnDoe'
        ]);

        $this->assertEquals(['JaneDoe', 'JohnDoe'], $reply->mentionedUsers());
    }

    public function testItWrapsMentionedUsernamesInTheBodyWithinAnchorTags()
    {
        $reply = create('Reply', [
            'body' => 'Hello @jane-doe.'
        ]);

        $this->assertEquals(
            'Hello <a href="/profiles/jane-doe">@jane-doe</a>.',
            $reply->body
        );
    }

    public function testItKnowsIfItIsTheBestReply()
    {
        $reply = create('Reply');

        $this->assertFalse($reply->isBest());

        $reply->thread->update(['best_reply_id' => $reply->id]);

        $this->assertTrue($reply->fresh()->isBest());
    }

    public function testAReplyBodyIsSanitizedAutomatically()
    {
        $reply = make('Reply', ['body' => '<script>alert("bad")</script><p>::some-text::</p>']);

        $this->assertEquals("<p>::some-text::</p>", $reply->body);
    }
}
