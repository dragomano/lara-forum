<?php

namespace Tests\Unit;

use App\Notifications\ThreadWasUpdated;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class ThreadTest extends TestCase
{
    protected $thread;

    public function setUp(): void
    {
        parent::setUp();

        $this->thread = create('Thread');
    }

    public function testAThreadHasAPath()
    {
        $this->assertEquals(
            "/threads/{$this->thread->channel->slug}/{$this->thread->slug}",
            $this->thread->path()
        );
    }

    public function testAThreadHasACreator()
    {
        $this->assertInstanceOf('App\Models\User', $this->thread->creator);
    }

    public function testAThreadHasReplies()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->thread->replies);
    }

    public function testAThreadCanAddAReply()
    {
        $this->thread->addReply([
           'body' => '::some-text::',
           'user_id' => 1
        ]);

        $this->assertCount(1, $this->thread->replies);
    }

    public function testAThreadNotifiesAllRegisteredSubscribersWhenAReplyIsAdded()
    {
        Notification::fake();

        $this->signIn();

        $this->thread->subscribe()->addReply([
            'body' => '::some-text::',
            'user_id' => 1
        ]);

        try {
            Notification::assertSentTo(auth()->user(), ThreadWasUpdated::class);
        } catch (\Exception $e) {
            $this->fail(__('messages.tests.notification_failed'));
        }
    }

    public function testAThreadBelongsToAChannel()
    {
        $this->assertInstanceOf('App\Models\Channel', $this->thread->channel);
    }

    public function testAThreadCanBeSubscribedTo()
    {
        $this->thread->subscribe($userId = 1);

        $this->assertEquals(1, $this->thread->subscriptions()->where('user_id', $userId)->count());
    }

    public function testAThreadCanBeUnsubscribedFrom()
    {
        $this->thread->subscribe($userId = 1);

        $this->assertCount(1, $this->thread->subscriptions);

        $this->thread->unsubscribe($userId);

        $this->assertCount(0, $this->thread->fresh()->subscriptions);
    }

    public function testItKnowsIfAUserIsSubscribedToIt()
    {
        $this->signIn();

        $this->assertFalse($this->thread->isSubscribedTo);

        $this->thread->subscribe();

        $this->assertTrue($this->thread->isSubscribedTo);
    }

    public function testAThreadCanCheckIfAUserHasReadAllReplies()
    {
        $this->signIn();

        tap(auth()->user(), function ($user) {
            $this->assertTrue($this->thread->hasUpdatesFor($user));

            $user->read($this->thread);

            $this->assertFalse($this->thread->hasUpdatesFor($user));
        });
    }

    public function testAThreadBodyIsSanitizedAutomatically()
    {
        $thread = make('Thread', ['body' => '<script>alert("bad")</script><p>::some-text::</p>']);

        $this->assertEquals("<p>::some-text::</p>", $thread->body);
    }
}
