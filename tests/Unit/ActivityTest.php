<?php

namespace Tests\Unit;

use App\Models\Activity;
use Tests\TestCase;

class ActivityTest extends TestCase
{
    public function testItRecordsActivityWhenAThreadIsCreated()
    {
        $this->signIn();

        $thread = create('Thread');

        $this->assertDatabaseHas('activities', [
            'user_id' => auth()->id(),
            'subject_id' => $thread->id,
            'subject_type' => 'App\Models\Thread',
            'type' => 'created_thread'
        ]);

        $activity = Activity::first();

        $this->assertEquals($activity->subject->id, $thread->id);
    }

    public function testItRecordsActivityWhenAReplyIsCreated()
    {
        $this->signIn();

        create('Reply');

        $this->assertEquals(2, Activity::count());
    }

    public function testItFetchesAFeedForAnyUser()
    {
        $this->signIn();

        create('Thread', ['user_id' => auth()->id()], 2);

        auth()->user()->activity()->first()->update([
            'created_at' => now()->subWeek()
        ]);

        $feed = Activity::feed(auth()->user());

        $this->assertTrue($feed->keys()->contains(
            now()->format('Y-m-d')
        ));

        $this->assertTrue($feed->keys()->contains(
            now()->subWeek()->format('Y-m-d')
        ));
    }
}
