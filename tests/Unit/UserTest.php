<?php

namespace Tests\Unit;

use Database\Seeders\RolesTableSeeder;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testMembersCanFetchTheirMostRecentReply()
    {
        $user = create('User');

        $reply = create('Reply', ['user_id' => $user->id]);

        $this->assertEquals($reply->id, $user->lastReply->id);
    }

    public function testMembersCanDetermineTheirAvatarPath()
    {
        $user = create('User');

        $this->assertEquals(asset('storage/avatars/default.png'), $user->avatar_path);

        $user->avatar_path = 'avatars/me.jpg';

        $this->assertEquals(asset('storage/avatars/me.jpg'), $user->avatar_path);
    }

    public function testEachUserHasARole()
    {
        $user = create('User');

        $this->seed(RolesTableSeeder::class);

        $this->assertNotEmpty($user->role);
    }
}

