<?php

function create($model, $attributes = [], $times = null)
{
    $class = 'App\Models\\' . $model;

    return $class::factory($times)->create($attributes);
}

function make($model, $attributes = [], $times = null)
{
    $class = 'App\Models\\' . $model;

    return $class::factory($times)->make($attributes);
}
