<?php

namespace Tests\Feature;

use Tests\TestCase;

class NotificationTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    public function testANotificationIsPreparedWhenASubscribedThreadReceivesANewReplyThatIsNotByTheCurrentUser()
    {
        $thread = create('Thread')->subscribe();

        $this->assertCount(0, auth()->user()->notifications);

        $thread->addReply([
            'user_id' => create('User')->id,
            'body' => '::some-text::'
        ]);

        $this->assertCount(1, auth()->user()->fresh()->notifications);
    }

    public function testAUserCanFetchTheirUnreadNotifications()
    {
        $thread = create('Thread')->subscribe();

        $thread->addReply([
            'user_id' => create('User')->id,
            'body' => '::some-text::'
        ]);

        $response = $this->getJson("/profiles/" . auth()->user() . "/notifications")->json();

        $this->assertCount(1, $response);
    }

    public function testAUserCanMarkANotificationAsRead()
    {
        $thread = create('Thread')->subscribe();

        $thread->addReply([
            'user_id' => create('User')->id,
            'body' => '::some-text::'
        ]);

        $user = auth()->user();

        $this->assertCount(1, $user->unreadNotifications);

        $notificationId = $user->unreadNotifications->first()->id;

        $this->delete("/profiles/{$user->slug}/notifications/{$notificationId}");

        $this->assertCount(0, $user->fresh()->unreadNotifications);
    }
}
