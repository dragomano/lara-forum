<?php

namespace Tests\Feature;

use App\Models\Thread;
use Tests\TestCase;

class SearchTest extends TestCase
{
    public function testMembersCanSearchThreads()
    {
        config(['scout.driver' => 'algolia']);

        $search = 'foobar';

        create('Thread', [], 2);
        create('Thread', ['body' => "A thread with the {$search} term."], 2);

        do {
            sleep(.15);

            $results = $this->getJson("/threads/search?q={$search}")->json()['data'];
        } while (empty($results));

        $this->assertCount(2, $results);

        Thread::latest()->take(4)->unsearchable();
    }
}
