<?php

namespace Tests\Feature;

use Tests\TestCase;

class BestReplyTest extends TestCase
{
    public function testAThreadCreatorMayMarkAnyReplayAsTheBestReply()
    {
        $this->signIn();

        $thread = create('Thread', ['user_id' => auth()->id()]);

        $replies = create('Reply', ['thread_id' => $thread->id], 2);

        $this->assertFalse($replies[1]->isBest());

        $this->postJson(route('bestReplies.store', [$replies[1]->id]));

        $this->assertTrue($replies[1]->fresh()->isBest());
    }

    public function testOnlyTheThreadCreatorMayMarkAReplyAsBest()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = create('Thread', ['user_id' => auth()->id()]);

        $replies = create('Reply', ['thread_id' => $thread->id], 2);

        $this->signIn(create('User'));

        $this->postJson(route('bestReplies.store', [$replies[1]->id]))
            ->assertStatus(403);

        $this->assertFalse($replies[1]->fresh()->isBest());
    }

    public function testIfABestReplyIsDeletedThenTheThreadIsProperlyUpdatedToReflectThat()
    {
        $this->signIn();

        $reply = create('Reply', ['user_id' => auth()->id()]);

        $reply->thread->markBestReply($reply);

        $this->deleteJson(route('replies.destroy', $reply));

        $this->assertNull($reply->fresh());
    }
}

