<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProfileTest extends TestCase
{
    public function testAllMembersHaveProfiles()
    {
        $user = create('User');

        $this->get("/profiles/{$user->slug}")
            ->assertSee($user->name);
    }

    public function testProfilesDisplayAllThreadsCreatedByTheAssociatedUser()
    {
        $this->signIn();

        $thread = create('Thread', ['user_id' => auth()->id()]);

        $this->get('/profiles/' . auth()->user()->slug)
            ->assertSee($thread->title)
            ->assertSee($thread->body);
    }
}
