<?php

namespace Tests\Feature;

use Tests\TestCase;

class UpdateThreadsTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    public function testAThreadCanBeUpdatedByItsCreatorOnly()
    {
        $thread = create('Thread', ['user_id' => auth()->id()]);

        $this->patch($thread->path(), [
            'title' => '::updated::',
            'body' => '::updated::'
        ]);

        tap($thread->fresh(), function ($thread) {
            $this->assertEquals('::updated::', $thread->title);
            $this->assertEquals('::updated::', $thread->body);
        });
    }

    public function testAThreadRequiresATitleAndBodyToBeUpdated()
    {
        $thread = create('Thread', ['user_id' => auth()->id()]);

        $this->patch($thread->path(), [
            'title' => '::some-text::'
        ])->assertSessionHasErrors('body');

        $this->patch($thread->path(), [
            'body' => '::some-text::'
        ])->assertSessionHasErrors('title');
    }

    public function testGuestsCannotUpdateThreads()
    {
        $thread = create('Thread', ['user_id' => create('User')->id]);

        $this->patch($thread->path(), [])->assertStatus(403);
    }
}
