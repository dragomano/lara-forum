<?php

namespace Tests\Feature;

use App\Models\Trending;
use Tests\TestCase;

class TrendingThreadsTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->trending = new Trending();

        $this->trending->reset();
    }

    public function testItIncrementsAThreadsScoreEachTimeItIsRead()
    {
        $this->assertEmpty($this->trending->get());

        $thread = create('Thread');

        $this->call('GET', $thread->path());

        $this->assertCount(1, $trending = $this->trending->get());

        $this->assertEquals($thread->title, $trending[0]['title']);
    }
}

