<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    public function testAConfirmationEmailIsSentUponRegistration()
    {
        Notification::fake();

        create('User');

        Notification::assertNothingSent();

        $user = User::factory()->unverified()->create();

        $user->sendEmailVerificationNotification();

        Notification::assertSentTo($user, VerifyEmail::class);
    }

    public function testMembersCanFullyConfirmTheirEmailAddresses()
    {
        $user = User::factory()->unverified()->create();

        $this->assertNull($user->email_verified_at);

        $user->markEmailAsVerified();

        $this->assertNotNull($user->email_verified_at);
    }
}

