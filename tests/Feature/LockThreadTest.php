<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class LockThreadTest extends TestCase
{
    public function testNonAdminsCannotLockThreads()
    {
        $this->signIn();

        $thread = create('Thread', ['user_id' => auth()->id()]);

        $this->post(route('threads.lock', $thread), [
            'locked' => true
        ])->assertStatus(403);

        $this->assertFalse(!! $thread->fresh()->locked);
    }

    public function testAdminsCanLockThreads()
    {
        $this->signIn(User::factory()->admin()->create());

        $thread = create('Thread', ['user_id' => auth()->id()]);

        $this->post(route('threads.lock', $thread));

        $this->assertTrue($thread->fresh()->locked, __('messages.tests.thread_was_locked'));
    }

    public function testAdminsCanUnlockThreads()
    {
        $this->signIn(User::factory()->admin()->create());

        $thread = create('Thread', ['user_id' => auth()->id(), 'locked' => false]);

        $this->delete(route('threads.unlock', $thread));

        $this->assertFalse($thread->fresh()->locked);
    }

    public function testOnceLockedAThreadMayNotReceiveNewReplies()
    {
        $this->signIn();

        $thread = create('Thread', ['locked' => true]);

        $this->post($thread->path() . '/replies', [
            'body' => '::some-text::',
            'user_id' => auth()->id()
        ])->assertStatus(422);
    }
}

