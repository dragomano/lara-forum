<?php

namespace Tests\Feature;

use Tests\TestCase;

class ParticipateInThreadsTest extends TestCase
{
    public function testGuestsCannotAddReplies()
    {
        $this->withExceptionHandling()
            ->post('/threads/some-channel/1/replies', [])
            ->assertRedirect('/login');
    }

    public function testMembersCanParticipateInForumThreads()
    {
        $this->signIn();

        $thread = create('Thread');
        $reply = make('Reply');

        $this->post($thread->path() . '/replies', $reply->toArray());

        $this->assertDatabaseHas('replies', ['body' => $reply->body]);
        $this->assertEquals(1, $thread->fresh()->replies_count);
    }

    public function testAReplyRequiredABody()
    {
        $this->withExceptionHandling()
            ->signIn();

        $thread = create('Thread');
        $reply = make('Reply', ['body' => null]);

        $this->json('post', $thread->path() . '/replies', $reply->toArray())
            ->assertStatus(422);
    }

    public function testGuestsCannotDeleteReplies()
    {
        $this->withExceptionHandling();

        $reply = create('Reply');

        $this->delete("/replies/{$reply->id}")
            ->assertRedirect('/login');

        $this->signIn()
            ->delete("/replies/{$reply->id}")
            ->assertStatus(403);
    }

    public function testGuestsCannotUpdateReplies()
    {
        $this->withExceptionHandling();

        $reply = create('Reply');

        $this->patch("/replies/{$reply->id}")
            ->assertRedirect('/login');

        $this->signIn()
            ->patch("/replies/{$reply->id}")
            ->assertStatus(403);
    }

    public function testMembersCanDeleteReplies()
    {
        $this->signIn();

        $reply = create('Reply', ['user_id' => auth()->id()]);

        $this->delete("/replies/{$reply->id}")
            ->assertStatus(302);

        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
        $this->assertEquals(0, $reply->thread->fresh()->replies_count);
    }

    public function testMembersCanUpdateReplies()
    {
        $this->signIn();

        $reply = create('Reply', ['user_id' => auth()->id()]);

        $updatedReply = '::updated-text::';
        $this->patch("/replies/{$reply->id}", ['body' => $updatedReply]);

        $this->assertDatabaseHas('replies', ['id' => $reply->id, 'body' => $updatedReply]);
    }

    public function testRepliesThatContainSpamMayNotBeCreated()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = create('Thread');

        $reply = make('Reply', [
            'body' => 'Yahoo Customer Support'
        ]);

        $this->json('post', $thread->path() . '/replies', $reply->toArray())
            ->assertStatus(422);
    }

    public function testMembersCanReplyAMaximumOfOncePerMinuteOnly()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = create('Thread');

        $reply = make('Reply', [
            'body' => '::some-text::'
        ]);

        $this->post($thread->path() . '/replies', $reply->toArray())
            ->assertStatus(201);

        $this->post($thread->path() . '/replies', $reply->toArray())
            ->assertStatus(429);
    }
}
