<?php

namespace Tests\Feature;

use Tests\TestCase;

class FavoritesTest extends TestCase
{
    public function testGuestsCannotFavoriteAnything()
    {
        $this->withExceptionHandling()
            ->post('replies/1/favorites')
            ->assertRedirect('/login');
    }

    public function testMembersCanFavoriteAnyReply()
    {
        $this->signIn();

        $reply = create('Reply');

        $this->post('replies/' . $reply->id . '/favorites');

        $this->assertCount(1, $reply->favorites);
    }

    public function testMembersCanUnfavoriteAReply()
    {
        $this->signIn();

        $reply = create('Reply');

        $reply->favorite();

        $this->delete('replies/' . $reply->id . '/favorites');

        $this->assertCount(0, $reply->favorites);
    }

    public function testMembersCanFavoriteAReplyOnlyOnce()
    {
        $this->signIn();

        $reply = create('Reply');

        try {
            $this->post('replies/' . $reply->id . '/favorites');
            $this->post('replies/' . $reply->id . '/favorites');
        } catch (\Exception $e) {
            $this->fail(__('messages.tests.only_one_favorite_reply'));
        }

        $this->assertCount(1, $reply->favorites);
    }
}

