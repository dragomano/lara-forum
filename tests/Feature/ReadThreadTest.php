<?php

namespace Tests\Feature;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;

class ReadThreadTest extends TestCase
{
    /**
     * @var Collection|Model|mixed
     */
    private $thread;

    public function setUp(): void
    {
        parent::setUp();

        $this->thread = create('Thread');
    }

    public function testCanViewAllThreads()
    {
        $this->get('/threads')
            ->assertSee($this->thread->title);
    }

    public function testCanViewASingleThread()
    {
        $this->get($this->thread->path())
            ->assertSee($this->thread->title);
    }

    public function testMembersCanFilterThreadsAccordingToAChannel()
    {
        $channel = create('Channel');
        $threadInChannel = create('Thread', ['channel_id' => $channel->id]);
        $threadNotInChannel = create('Thread');

        $this->get('/threads/' . $channel->slug)
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);
    }

    public function testMembersCanFilterThreadsByAnyUsername()
    {
        $user = create('User');

        $this->signIn($user);

        $threadByJohn = create('Thread', ['user_id' => auth()->id()]);

        $this->get('threads?by=' . $user->slug)
            ->assertSee($threadByJohn->title)
            ->assertDontSee($this->thread->title);
    }

    public function testMembersCanFilterThreadsByPopularity()
    {
        $threadWithTwoReplies = create('Thread');
        create('Reply', ['thread_id' => $threadWithTwoReplies->id], 2);

        $threadWithThreeReplies = create('Thread');
        create('Reply', ['thread_id' => $threadWithThreeReplies->id], 3);

        $response = $this->getJson('threads?popular=1')->json();
        $this->assertEquals([3, 2, 0], array_column($response['data'], 'replies_count'));
    }

    public function testMembersCanFilterThreadsByThoseThatAreUnanswered()
    {
        create('Reply', ['thread_id' => create('Thread')->id]);

        $response = $this->getJson('threads?unanswered=1')->json();

        $this->assertCount(1, $response['data']);
    }

    public function testMembersCanRequestAllRepliesForAGivenThread()
    {
        create('Reply', ['thread_id' => $this->thread->id], 2);

        $response = $this->getJson($this->thread->path() . '/replies')->json();

        $this->assertCount(2, $response['data']);
        $this->assertEquals(2, $response['total']);
    }

    public function testWeRecordANewVisitEachTimeTheThreadIsRead()
    {
        $this->assertSame(0, $this->thread->visits);

        $this->call('GET', $this->thread->path());

        $this->assertEquals(1, $this->thread->fresh()->visits);
    }
}
