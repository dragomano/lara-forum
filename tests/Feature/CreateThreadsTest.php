<?php

namespace Tests\Feature;

use App\Models\Activity;
use App\Rules\Recaptcha;
use Tests\TestCase;

class CreateThreadsTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        app()->singleton(Recaptcha::class, function () {
            return \Mockery::mock(Recaptcha::class, function ($m) {
                $m->shouldReceive('passes')->andReturn(true);
            });
        });
    }

    public function publishThread($overrides = [])
    {
        $this->withExceptionHandling()->signIn();

        $thread = make('Thread', $overrides);

        return $this->post('/threads', $thread->toArray() + ['g-recaptcha-response' => 'token']);
    }

    public function testGuestsCannotCreateThreads()
    {
        $this->withExceptionHandling();

         $this->get('/threads/create')
             ->assertRedirect('/login');

         $this->post('/threads')
             ->assertRedirect('/login');
    }

    public function testMembersMustFirstConfirmTheirEmailAddressBeforeCreatingThreads()
    {
        $this->publishThread()
            ->assertStatus(302);
    }

    public function testMembersCanCreateNewForumThreads()
    {
        $response = $this->publishThread(['title' => '::some title::', 'body' => '::some body::']);

        $this->get($response->headers->get('Location'))
            ->assertSee('::some title::')
            ->assertSee('::some body::');
    }

    public function testGuestsCannotDeleteThreads()
    {
        $this->withExceptionHandling();

        $thread = create('Thread');

        $this->delete($thread->path())
            ->assertRedirect('/login');

        $this->signIn();

        $this->delete($thread->path())
            ->assertStatus(403);
    }

    public function testMembersCanDeleteThreads()
    {
        $this->signIn();

        $thread = create('Thread', ['user_id' => auth()->id()]);

        $reply = create('Reply', ['thread_id' => $thread->id]);

        $response = $this->json('DELETE', $thread->path());

        $response->assertStatus(204);

        $this->assertDatabaseMissing('threads', ['id' => $thread->id]);

        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);

        $this->assertEquals(0, Activity::count());
    }

    public function testAThreadRequiresATitle()
    {
        $this->publishThread(['title' => null])
            ->assertSessionHasErrors('title');
    }

    public function testAThreadRequiresABody()
    {
        $this->publishThread(['body' => null])
            ->assertSessionHasErrors('body');
    }

    public function testAThreadRequiresAValidChannel()
    {
        create('Channel', [], 2);

        $this->publishThread(['channel_id' => null])
            ->assertSessionHasErrors('channel_id');

        $this->publishThread(['channel_id' => 999])
            ->assertSessionHasErrors('channel_id');
    }

    public function testAThreadRequiresAnUniqueSlug()
    {
        $this->signIn();

        $thread = create('Thread', ['title' => 'Foo Title']);

        $this->assertEquals('foo-title', $thread->fresh()->slug);

        $thread = $this->postJson(
            route('threads.store'),
            $thread->toArray() + ['g-recaptcha-response' => 'token']
        )->json();

        $this->assertEquals("foo-title-{$thread['id']}", $thread['slug']);
    }

    public function testAThreadWithATitleThatEndsInANumberShouldGenerateTheProperSlug()
    {
        $this->signIn();

        $thread = create('Thread', ['title' => 'Some Title 24']);

        $thread = $this->postJson(
            route('threads.store'),
            $thread->toArray() + ['g-recaptcha-response' => 'token']
        )->json();

        $this->assertEquals("some-title-24-{$thread['id']}", $thread['slug']);
    }

    public function testAThreadRequiresRecaptchaVerification()
    {
        unset(app()[Recaptcha::class]);

        $this->publishThread(['g-recaptcha-response' => 'test'])
            ->assertSessionHasErrors('g-recaptcha-response');
    }
}
