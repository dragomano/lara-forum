<?php

namespace Tests\Feature;

use Tests\TestCase;

class MentionUsersTest extends TestCase
{
    public function testMentionedUsersInAReplyAreNotified()
    {
        $this->signIn();

        $jane = create('User', ['name' => 'JaneDoe']);

        $thread = create('Thread');

        $reply = make('Reply', [
            'body' => '@JaneDoe look at this.'
        ]);

        $this->json('post', $thread->path() . '/replies', $reply->toArray());

        $this->assertCount(1, $jane->notifications);
    }

    public function testItCanFetchAllMentionedUsersStartingWithTheGivenCharacters()
    {
        create('User', ['name' => 'johndoe']);
        create('User', ['name' => 'johndoe2']);
        create('User', ['name' => 'janedoe']);

        $results = $this->json('GET', '/api/users', ['name' => 'john']);

        $this->assertCount(2, $results->json());
    }
}
